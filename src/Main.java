
import genes.Fasta;
import matrix.*;
import mpi.MPI;


public class Main {

    public static void main(String[] args) {
        //Zadania z macierzami


        Hadamard hadamard = new Hadamard(16);
        hadamard.printHadamardMatrix();
        System.out.println("Is Hadamard: " + hadamard.isHadamardMatrix());
        int matrixLevel = 4;
        Matrix a = new Matrix(matrixLevel);
        Matrix b = new Matrix(matrixLevel);

        System.out.println("---------------A-----------------");
        a.printMatrix();
        System.out.println("---------------B-----------------");
        b.printMatrix();

        Matrix c = new Matrix(Matrix.multiplyMatrix(a.getMatrix(), b.getMatrix()));
        System.out.println("---------------C-----------------");
        c.printMatrix();

        c.transposeMatrix();
        System.out.println("---------------CT-----------------");
        c.printMatrix();

        Matrix d = new Matrix(c.powerMatrix(2));
        System.out.println("---------------C^10-----------------");
        d.printMatrix();


       // Sekcja MPI

//        MPI.Init(args);
//
//        int myrank = MPI.COMM_WORLD.Rank();
//        int size = MPI.COMM_WORLD.Size() ;
//        System.out.println("Hello world from rank " + myrank + " of " + size);
//
//        MPI.Finalize();
//
//        Zadania z genami
            //Fasta f = new Fasta(args);

    }
}
