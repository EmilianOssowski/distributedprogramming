package matrix;


import com.sun.org.apache.xpath.internal.operations.Bool;

public class Hadamard {
    Boolean [][] matrix;
    int size;
    public Hadamard (int row) {
        generatHadamarMatrix(row);
    }

    private void generatHadamarMatrix(int row){
        size = row * row;
        matrix = new Boolean[row][row];
        matrix[0][0] = true;
        for(int n = 1; n < row; n += n){
            for (int i = 0; i < n; i++) {
                for(int j = 0; j < n; j++) {
                    matrix[i + n][j] = matrix[i][j];
                    matrix[i][j + n] = matrix[i][j];
                    matrix[i + n][j + n] = !matrix[i][j];
                }
            }
        }


    }

    public Boolean isHadamardMatrix(){
        Boolean [][] tran = transposeMatrix(matrix);

        for(int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[0].length; j++){
                if(tran[i][j] != matrix[i][j]){
                    return false;
                }
            }
        }

        return true;
    }

    public Boolean[][] transposeMatrix(Boolean [][] matrix) {
        Boolean[][] tran = new Boolean[matrix.length][matrix.length];
        for(int i = 0; i < matrix.length; i++)
        {
            for(int j = 0;j < matrix.length; j++)
                tran[j][i] = matrix[i][j];
        }
        return tran;
    }
    public void printHadamardMatrix(){
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[0].length; j++){
                if(matrix[i][j] == true)
                    System.out.print("T ");
                else
                    System.out.print("F ");
            }
            System.out.print('\n');
        }
    }
}
