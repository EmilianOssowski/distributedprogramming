package matrix;

import java.util.Random;

public class Matrix {
    int[][] matrix;

    public Matrix(int row) {
        Random generator = new Random();

        matrix = new int[row][row];
        for (int y = 0; y < row; y++) {
            for (int x = 0; x < row; x++) {
                matrix[y][x] = generator.nextInt(2);
            }
        }
    }

    public Matrix(int[][] m) {
        matrix = m;
    }

    public void printMatrix() {
        for (int y = 0; y < matrix.length; y++) {
            for (int x = 0; x < matrix[0].length; x++) {
                System.out.print(matrix[y][x] + ", ");
            }
            System.out.print('\n');
        }
        System.out.print("\n");
    }

    public static int[][] multiplyMatrix(int[][] a, int[][] b) {
        int[][] c = new int[a.length][b.length];

        for (int y = 0; y < a.length; y++) {
            for (int x = 0; x < a[0].length; x++) {
                c[y][x] = 0;
                for (int j = 0; j < b.length; j++) {
                    c[y][x] += a[y][j] * b[j][x];
                }
            }
        }
        return c;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void transposeMatrix() {
        int[][] res = new int[matrix.length][matrix.length];
        for(int i = 0; i < matrix.length; i++)
        {
            for(int j = 0;j < matrix.length; j++)
                res[j][i] = matrix[i][j];
        }

        matrix = res;
    }

    public int[][] powerMatrix(int pow){
        if(pow == 0 )
            return multiplyMatrix(matrix, matrix );
        return powerMatrix(pow-1);
    }

}
