package genes;


import mpi.MPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;

public class Fasta {

    String header;


    public Fasta(String[] args) {


        StringBuilder h;
        StringBuilder dnaSeq;
        try {
            URL url = new URL("https://www.ebi.ac.uk/ena/data/view/BN000001&display=fasta");
            h = new StringBuilder();
            dnaSeq = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
                h.append(reader.readLine());
                for (String line; (line = reader.readLine()) != null; ) {
                    dnaSeq.append(line.replace("N", ""));
                }
            }

            MPI.Init(args);

            int myrank = MPI.COMM_WORLD.Rank();
            int mysize = MPI.COMM_WORLD.Size();
            int range = 60;
            int floor = 15;

            header = h.toString();
            String dna = dnaSeq.toString();
            int fileSize = dnaSeq.length();
            int start = myrank * (fileSize / mysize);
            int end = myrank != (mysize - 1) ? (myrank + 1) * (fileSize / mysize)+ range : (myrank + 1) * (fileSize / mysize) + 1  ;
            findLTR(dna, start, end, myrank,mysize, range, floor);
           // System.out.println("P" + myrank + " Start " + start + " End " + end);
            MPI.Finalize();
        } catch (IOException e) {
            System.out.println("error : " + e);
        }

    }

    private void findLTR(String dna, int start, int stop, int myrank, int mysize, int range, int floor) {
        int counter = 0;

        //Set<String> sequences = new HashSet<>();
        Map<String, Integer> occurances = new HashMap<String, Integer>();
        String maxSeq = "";
        StringBuilder actualSequence = new StringBuilder();
        long time_start = System.currentTimeMillis();
        for (int j = range; j > floor; j--) { // j - długosć najdłuższej szukanej sekwencji
            for (int i = start; i < stop - j; i++) {
                actualSequence.setLength(0);
                for (int k = i; k < i + j; k++) {
                    actualSequence.append(dna.charAt(k));
                }
                if (dna.contains(actualSequence)) {
//                    System.out.println(actualSequence);
                    if (occurances.containsKey(actualSequence.toString())) {

                        Integer newOcc = occurances.get(actualSequence.toString()) + 1;
                        occurances.replace(actualSequence.toString(), newOcc);
                    } else {
                        occurances.put(actualSequence.toString(), 0);
                    }

                }
            }
        }
        String longestSeq = "";
        for (String seq : occurances.keySet()) {
            if (occurances.get(seq) > 1) {
                if (seq.length() > longestSeq.length()) {
                    longestSeq = seq;
                }

            }
        }
        long time_end = System.currentTimeMillis();
        System.out.println("P" + myrank + " start time: " + new Date(time_start) + " time end: " + new Date(time_end) + " time elapsed:" + (time_end - time_start)+ " Found sequence: " +  longestSeq + " Length: " + longestSeq.length() );


        String[] results = new String[mysize];
        String[] sendTab = new String[1];
        sendTab[0] = longestSeq;

        //if(myrank == 0){
        MPI.COMM_WORLD.Barrier();
        //}
        MPI.COMM_WORLD.Gather(sendTab, 0, 1, MPI.OBJECT, results, 0, 1, MPI.OBJECT, 0);
        if (myrank == 0) {
            String longest = "";
            int index = 0;
            for (int i = 0; i < results.length; i++) {
                System.out.println("P" + i + ": " + results[i]);

                if (results[i].length() > longest.length()) {
                    longest = results[i];
                    index = i;
                }

            }
            System.out.println("P" + index + " found longest sequence: " + longest );



        }
    }
}
